<!-- navbar -->
<div class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="products.php">XYZ Webstore</a>
        </div>

        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav"> 
                <!-- highlight if $page_title has 'Products' word. -->
                 <li <?= strpos($page_title, "Products")!== false? "class='active'" : ""; ?>>
                    <a href="products.php">Products</a>
                </li>             

                <li <?= $page_title=="Cart" ? "class='active'" : ""; ?>>
                  <a href="cart.php">
                    <?php 
                    // count products in cart
                    if(isset($_SESSION['user_id'])) {
                    $cart_item->user_id = $_SESSION['user_id']; 
                    $cart_count = $cart_item->count() ;
                    ?>
                    Cart <span class="badge" id="comparison-count"><?= $cart_count; ?></span>
                <?php } ?>
                  </a>
                </li>
                <li>
                ログイン
                <form ation="" method="post">
                    <input type="text" name="user_name" placeholder="ユーザー名">
                    <input type="password" name="user_password" placeholder="パスワード">
                    <input type="submit" value="ログイン">
                </form>
                <?php
                // include objects
                include_once "objects/user.php";

                $user = new User($db);

                //ユーザー認証機能
                if(isset($_POST['user_name'], $_POST['user_password'])) {
                    $user->user_name = $_POST['user_name'];
                    $user->user_password = $_POST['user_password'];    
                    if($user->validate_credentials() === true) {
                        echo "ログインしました。";
                    } else {
                        echo "ユーザー名とパスワードが一致しません。";
                    }
                } // if(isset..)

                if(isset($_SESSION['user_name'])){
                    echo "こんにちは、" . $_SESSION['user_name'] . "さん";
                }

                 ?>
                </li>
                <li>
                <?php if(isset($_SESSION['user_id'])){ echo "<a href='logout.php'>ログアウト</a>";} ?>    
                
                </li> 
                <li>
                <a href="signin.php">会員登録</a>  
                </li>  
            </ul>
        </div>
    </div>
</div>


                                            