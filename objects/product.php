<?php
// 'product' object
class Product {
	// database connection and table name
	private $conn;
	private $table_name = "products";

	// object properties
	public $id;
	public $name;
	public $price;
	public $description;
	public $category_id;
	public $category_name;
	public $timestamp;

	//constructor
	public function __construct($db) {
		$this->conn = $db;
	} //function constructor

	// read all products
	function read($from_record_num, $records_per_page) {

		// select all products query
		$query = "SELECT id, name, description, price
			From " . $this->table_name . "
			 ORDER BY created DESC LIMIT ?, ?";

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind limit clause vriables
		$stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);

		// execute query
		$stmt->execute();

		// return values
		return $stmt;
	} // function read

	// use for paging products
	public function count(){
		// query to count all products records
		$query = "select count(*) FROM " . $this->table_name;

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// execute query
		$stmt->execute();

		// get row value
		$rows = $stmt->fetch(PDO::FETCH_NUM);

		// return count
		return $rows[0];
	} // function count

	// read all product based on product ids included in the $ids variable
	// reference http://stackoverflow.com/a/10722827/827418
	public function readByIds($ids) {
		$ids_arr = str_repeat('?,', count($ids) - 1) . '?';
		// query to select products
		$query = "select id, name, price from " . $this->table_name . " where id in ({$ids_arr}) order by name";
		// prepare query statement
		$stmt->$this->conn->prepare($query);
		// execute query
		$stmt->execute($ids);
		// return values from database
		return $stmt;
	} // function readByIds

	// read when filling up the update product form
	function readOne() {
		// query to select single record
		$query = "select name, description, price
				  from " . $this->table_name . " where id =? limit 0,1";
	    // prepare query statement
	    $stmt = $this->conn->prepare($query);
	    // sanitize
	    $stmt->bindParam(1, $this->id);
	    // execute query
	    $stmt->execute();
	    // get row values
	    $row = $stmt->fetch(PDO::FETCH_ASSOC);

	    // assign retrieved row value to object properties
	    $this->name = $row['name'];
	    $this->description = $row['description'];
	    $this->price = $row['price'];
	} // function readOne

	//アップデート機能
	function update() {
		// $_SESSION['update_error'] = "";
		// sanitize
		$this->name = htmlspecialchars(strip_tags($this->name));
		$this->price = htmlspecialchars(strip_tags($this->price));
		$this->description = htmlspecialchars(strip_tags($this->description));

		$query = "update " . $this->table_name . " set name = :name, price = :price, description = :description where id = :id";	

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind variables
		$stmt->bindParam(":name", $this->name);
		$stmt->bindParam(":price", $this->price);
		$stmt->bindParam(":description", $this->description);	
		$stmt->bindParam(":id", $this->id);						

		// execute query
		$result = $stmt->execute();
		return $result;
	} // function update

	//商品追加機能
	function add() {
		// $_SESSION['update_error'] = "";
		// sanitize
		$this->name = htmlspecialchars(strip_tags($this->name));
		$this->price = htmlspecialchars(strip_tags($this->price));
		$this->description = htmlspecialchars(strip_tags($this->description));

		$query = "INSERT INTO " . $this->table_name . " (name, description, price, created, modified) VALUES (:name, :description, :price, UNIX_TIMESTAMP(), UNIX_TIMESTAMP())";	

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind variables
		$stmt->bindParam(":name", $this->name);
		$stmt->bindParam(":price", $this->price);
		$stmt->bindParam(":description", $this->description);												

		// execute query
		$result = $stmt->execute();
		return $result;
	} // function update

	function delete(){
		$query = "delete from " . $this->table_name . " where id = :id";	

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind variable
		$stmt->bindParam(":id", $this->id);		

		// execute query
		$result = $stmt->execute();
		return $result;
	} // function delete
		
} //class Product
