<?php 
// 'product image' object
class User{
	//database connection and table name
	private $conn;
	private $table_name = "users";

	//object properties
	public $user_id;
	public $user_name;
	public $user_password;

	// constructor
	public function __construct($db) {
		$this->conn = $db;
	}

	function test() {
		return $this->table_name;
	}

	//ログイン機能
	function validate_credentials() {
		// select query
		$query = "SELECT user_id, user_name, user_password FROM " . $this->table_name . " WHERE user_name = :user_name";

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->user_name = htmlspecialchars(strip_tags($this->user_name));
		$this->user_password = htmlspecialchars(strip_tags($this->user_password));			
		// bind user_name variable
		$stmt->bindParam(":user_name", $this->user_name);

		// execute query
		$stmt->execute();

		if($stmt->rowCount() != 1) {
			return false;
		} 

		//パスワード(暗号化済み)とユーザーIDの取り出し
		while ($row = $stmt->fetch()) {
			$db_hashed_pwd = $row['user_password'];
			$this->user_id = $row['user_id'];
		} //whilea

		//ハッシュ化されたパスワードがマッチするかどうかを確認
		if (password_verify($this->user_password, $db_hashed_pwd)) {
			$_SESSION['user_id'] = $this->user_id;
			$_SESSION['user_name'] = $this->user_name;	
			return true;
		} else {
			return false;
		}

	} // function validate_credentials

	//会員登録機能
	function signup() {
		// sanitize
		$this->user_name = htmlspecialchars(strip_tags($this->user_name));
		$this->user_password = htmlspecialchars(strip_tags($this->user_password));
		$this->user_password = password_hash($this->user_password, PASSWORD_DEFAULT);

		// 登録済みのユーザ名か確認
		$query = "select count(*) from users where user_name = :user_name";

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind user_name variable
		$stmt->bindParam(":user_name", $this->user_name);

		// execute query
		$stmt->execute();

		if($stmt->fetchColumn() != 0) {
			$_SESSION['error'] = "ユーザ名が重複しています。";
			return false;
		}

		// パスワードの書式を確認
		//  if (!preg_match('/\A[a-zA-Z0-9]+\z/', $this->user_password)) {
		// echo "パスワードが不正です。";
 	// 	return false;
 	// 	} 

		// insert query
		$query = "INSERT INTO users(user_name, user_password) VALUES(:user_name, :user_password)";	

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind user_name variable
		$stmt->bindParam(":user_name", $this->user_name);
		// bind user_password variable
		$stmt->bindParam(":user_password", $this->user_password);

		// execute query
		$result = $stmt->execute();

		return $result;
	} // function signup	

} // class User