<?php 

class Adminuser {
	//database connection and table name
	private $conn;
	private $table_name = "adminusers";

	//object properties
	public $user_id;
	public $user_name;
	public $user_password;

	// // constructor
	public function __construct($db) {
		$this->conn = $db;
	}

	//ログイン機能
	function validate_credentials() {
		// select query
		$query = "SELECT user_id, user_name, user_password FROM " . $this->table_name . " WHERE user_name = :user_name";

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->user_name = htmlspecialchars(strip_tags($this->user_name));
		$this->user_password = htmlspecialchars(strip_tags($this->user_password));			
		// bind user_name variable
		$stmt->bindParam(":user_name", $this->user_name);

		// execute query
		$stmt->execute();

		if($stmt->rowCount() != 1) {
			return false;
		} 

		//パスワード(暗号化済み)とユーザーIDの取り出し
		while ($row = $stmt->fetch()) {
			$db_hashed_pwd = $row['user_password'];
			$this->user_id = $row['user_id'];
		} //whilea

		//ハッシュ化されたパスワードがマッチするかどうかを確認
		if (password_verify($this->user_password, $db_hashed_pwd)) {
			$_SESSION['adminuser_id'] = $this->user_id;
			$_SESSION['adminuser_name'] = $this->user_name;	
			return true;
		} else {
			return false;
		}

	} // function validate_credentials

} // class adminuser