<?php
// include classes
include_once "config/database.php";
include_once "objects/product.php";
include_once "objects/product_image.php";
include_once "objects/cart_item.php";

// get database connection
$database = new Database();
$db = $database->getConnection();

// initialize objects
$product = new Product($db);
$product_image = new ProductImage($db);

// get ID of the product to be edited and action
$id = isset($_GET['id']) ? $_GET['id'] : die('ERROR: missing ID.');
$action = isset($_GET['action']) ? $_GET['action'] : "";
// set the id as product id property
$product->id = $id;

// to read single record product
$product->readOne();

// set page title
$page_title = $product->name;

$cart_item = new CartItem($db);

// set page title
$page_title = $product->name;

// include page header HTML
include_once 'layout_head.php';
?>

<div class='col-md-12'>
	<?php if($action=='added') { ?>
	  <div class='alert alert-info'>  
	  	Product was added to your cart!
	  </div>
	<?php } else if ($action == 'unable_to_add') { ?>
	  <div class='alert alert-info'>
	    Unable to add product to cart. Please contact Admin.
      </div>
    <?php } ?> 
</div>

<?php
// set product id
$product_image->product_id = $id;

// read all related product image
$stmt_product_image = $product_image->readByProductId();

// count all related product image
$num_product_image = $stmt_product_image->rowCount();
?>

<div class='col-md-1'>
<?php
	// if count is more than zero
	if ($num_product_image >0) {
		// loop through all product image
	 while ($row = $stmt_product_image->fetch(PDO::FETCH_ASSOC)) {
	 	// image name and source url
	 	$product_image_name = $row['name'];
	 	$source = "uploads/images/{$product_image_name}";
	 	echo "<img src='{$source}' class='product-img-thumb' data-img-id='{$row['id']}'>";
	 } // while
	 }	else {
	 	echo "No images.";
	 }
 ?>
</div>

<div class='col-md-4' id='product-img'>
<?php
// read all related product image
$stmt_product_image = $product_image->readByProductId();
$num_product_image = $stmt_product_image->rowCount();

// if count is more than zero
if($num_product_image > 0) {
	// loop through all product images
	$x = 0;
	while ($row=$stmt_product_image->fetch(PDO::FETCH_ASSOC)){
		// image name and source code
		$product_image_name = $row['name'];
		$source = "uploads/images/{$product_image_name}";
		$show_product_img = $x ==0? "display-block" : "display-none";
		echo "<a href='{$source}' target='_blank' id='product-img-{$row['id']}' class='product-img {$show_product_img}'>";
		echo "<img src='{$source}' style='width:100%;' >";
		echo "</a>";
		$x++;
	} // while
} else {
	echo "No images.";
}
echo "</div>";
?>

<!-- 10.9 Display product details -->
<div class='col-md-5'>
	<div class='product-detail'>Price:</div>
	<?= "<h4 class='m-b-10px price-description'>&#36;" . number_format($product->price, 2, '.', ',') . "</h4>"; ?>
	<div class='product-detail'>Product description:</div>

	<div class='m-b-10px'>
	<?php 
	 // make html
	 $page_description = htmlspecialchars_decode(htmlspecialchars_decode($product->description));
	 // show to user
	 echo $page_description;
	?>
    </div>

    <div class='product-detail'>Product category:</div>
    <?= "<div class='m-b-10px'>{$product->category_name}</div>" ?>
</div>

<!-- 10.10 Render 'Cart button' -->
<div class='col-md-2'>
<?php
// cart item settings
$cart_item->user_id = $_SESSION['user_id']; // we default to a user with ID "1" for now
$cart_item->product_id = $id;

// if product was already added in the cart
if($cart_item->exists()){
?>
	<div class='m-b-10px'>This product is already in your cart.</div>
	<a href='cart.php' class='btn btn-success w-100-pct'>Update Cart</a>
<?php	
} // if product was not adde to the cart yet
 else {
?>
	<form class='add-to-cart-form'>
		<!-- product id -->
		<?= "<div class='product-id display-none'>{$id}</div>" ?>
		<!-- select quantity -->
		<div class='m-b-10px f-w-b'>Quantity:</div>
		<input type='number' class='form-control m-b-10px cart-quantity' value='1' min='1'>
		<!-- enable add to cart button -->
		<button style='width:100%;' type='submit' class='btn btn-primary add-to-cart m-b-10px'>
		<span class='glyphicon glyphicon-shopping-cart'></span>Add to Cart</button>
	</form>
 <?php } ?>	
</div>
<?php
// include page footer HTML
include_once 'layout_foot.php';