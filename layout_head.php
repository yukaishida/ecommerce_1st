<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= isset($page_title) ? $page_title:"The Code of a Ninja"; ?></title>
	<!-- Bootstrap CSS -->
<!--     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

    <!-- custom css for users -->
    <link href="libs/css/user.css" rel="stylesheet" media="screen">
</head>
<body>

	<?php include 'navigation.php'; ?>
	<!-- container -->
	<div class="container">
		<div class="row">

		<div clss="col-md-12">
			<div class="page-header">
				<h1><?= isset($page_title)? $page_title : "The Code of a Ninja"; ?></h1>	
			</div>
		</div>