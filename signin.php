<!DOCTYPE HTML>
<html lang="ja">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>会員登録ページ</title>

    <!-- Bootstrap読み込み（スタイリングのため） -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

        <!-- custom css for users -->
     <link href="libs/css/user.css" rel="stylesheet" media="screen">
</head>
<body>
<!-- navbar -->
<div class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="products.php">XYZ Webstore</a>
        </div>

        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav"> 
                 <li class="active">
                    <a href="products.php">Products</a>
                </li>             
            </ul>
        </div>
    </div>
</div>  <!-- navbarここまで -->

<?php
// if( isset($_SESSION['user_name']) !== "") {
//  // ログイン済みの場合はリダイレクト
//  header("Location: products.php");
// }

// connect to database
include 'config/database.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// include objects
include_once "objects/user.php";

$user = new User($db);

// signupがPOSTされたときに下記を実行
if(isset($_POST['signup'])) {
    $user->user_name = $_POST['user_name'];
    $user->user_password = $_POST['user_password'];

    if($user->signup() === true) {
    echo "<div class='alert alert-success' role='alert'><p>登録しました</p></div>";
    } else {
    echo "<div class='alert alert-danger' role='alert'><p>{$_SESSION['error']}</p></div>";
    } // else 
 }   

?>

<div class="col-xs-6 col-xs-offset-3">

<form method="post">
    <h1>会員登録フォーム</h1>
    <div class="form-group">
        <input type="text" class="form-control" name="user_name" placeholder="ユーザー名" required />
    </div>
    <div class="form-group">
        <input type="password" class="form-control" name="user_password" placeholder="パスワード" required />
    </div>
    <button type="submit" class="btn btn-default" name="signup">会員登録する</button>
    <a href="login.php">ログインはこちら</a>
</form>
</div>　<!-- class="col-xs-6 col-xs-offset-3" -->

</body>
</html>
