		</div><!-- /row  -->
	</div><!-- /container  -->	
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>	
	<script>
		$(document).ready(function(){
			// add to cart button listener
			$('.add-to-cart-form').on('submit', function(){
				// info is in the table/single product layout
				var id = $(this).find('.product-id').text();
				var quantity = $(this).find('.cart-quantity').val();

				// redirect to add_to_cart.php, with parameter values to process the request
				window.location.href = "add_to_cart.php?id=" + id + "&quantity =" + quantity; 
				return false;
			});

			// update quantity button listener
			$('.update-quantity-form').on('submit', function(){
				// get basic information for updating the cart
				var id = $(this).find('.product-id').text();
				var quantity = $(this).find(' .cart-quantity').val();
				// redirect to udate_quantity.php, with parameter values to process the request
				window.location.href = "update_quantity.php?id=" + id + "&quantity=" + quantity;
				return false;
			});

			// logout button listener
			// $('.logout-form').on('submit', function(){
			// 	window.location.href = "logout.php";
			// 	return false;
			// });			

			$(document).on('mouseenter', '.product-img-thumb', function(){
				var data_img_id = $(this).attr('data-img-id');
				$('.product-img').hide();
				$('#product-img-'+data_img_id).show();
			});
		});
	</script>	

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>