<?php

ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL);

// connect to database
include 'config/database.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// include objects
include_once "objects/product.php";
include_once "objects/product_image.php";
include_once "objects/cart_item.php";

// initialize objects
$product = new Product($db);
$product_image = new ProductImage($db);
$cart_item = new CartItem($db);

// set page title
$page_title = "Products";
// page header html
include 'layout_head.php';
         
// content will be here

//layout footer code
include 'layout_foot.php';

//to prevent undefined index notice
$action = isset($_GET['action']) ? $_GET['action'] : "";

// for pagination purposes
$page = isset($_GET['page']) ? $_GET['page'] : 1; //page is the current page, if there's nothing set, default is page 1
$records_per_page = 6;  // set records or rows of data per page
$from_record_num = ($records_per_page * $page) - $records_per_page; // calculate for the query LIMIT clause

// read all products in the database readメソッドはproductクラスで定義
$stmt = $product->read($from_record_num, $records_per_page);

// count number of retrieved products
$num = $stmt->rowCount();
// if products retrieved were more than zero

if ($num > 0) {
	// needed for paging
	$page_url = "products.php?";
	$total_rows = $product->count();

	// show products
	include_once "read_products_template.php";
}

// tell the user if there's no products in the database
else {
	echo "<div class='col-md-12'>";
	echo "<div class='alert alert-danger'>No Products found.</div>";
	echo "</div>"; //class='col-md-12'
}

// $_SESSION = [];
// session_destroy();
