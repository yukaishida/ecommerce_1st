<?php
// connect to database
include 'config/database.php';

$_SESSION = [];
session_destroy();

header("Location: login.php");
