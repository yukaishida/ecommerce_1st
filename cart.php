<?php
// connect to database
include 'config/database.php';

// include objects
include_once "objects/product.php";
include_once "objects/product_image.php";
include_once "objects/cart_item.php";

// get database connection
$database = new Database();
$db = $database->getConnection();

// initialize objects
$product = new Product($db);
$product_image = new ProductImage($db);
$cart_item = new CartItem($db);

// set page title
$page_title = 'Cart';

// include page headeer html
include 'layout_head.php';

// content will be here
$action = isset($_GET['action']) ? $_GET['action'] : "";
?>

<div class='col-md-12'>
<?php if($action=='removed'){ ?>
	<div class='alert alert-info'>
	  <p>Products was removed from your cart!</p>
	</div>
<?php } else if ($action=='quantity_updated') { ?>
	<div class='alert alert-info'>
	  <p>Product quantity was updated!</p>
	</div>
<?php } else if($action=='exists') { ?>
	<div class='alert alert-info'>
	  <p>Producnt already exists in your cart!</p>
	</div>
<?php } else if ($action=='cart_emptied') { ?>
	<div class='alert alert-info'>
	  <p>Cart was emptied.</p>
	</div>
<?php } else if ($action=='updated') { ?>
	<div class='alert alert-info'>
	  <p>Quantity was updated.</p>
	</div>
<?php } else if ($action=='unale_to_update'){ ?>
	<div class='alert alert-danger'>
	  <p>Unable to update quantity.</p>
	</div>
<?php } ?>
</div>

<?php
// $cart count variable is initialized in navigation.php
if ($cart_count>0) {
	$cart_item->user_id = $_SESSION['user_id'];
	$stmt=$cart_item->read();
	$total = 0;
	$item_count = 0;

while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
	extract($row);
	$sub_total=$price*$quantity;
?>
<div class='cart-row'>
  <div class='col-md-8'>
	<!-- product name -->
	<div class='product-name m-b-10px'>
	<?= "<h4>{$name}</h4>" ?>
	</div>

	<!-- update quantity -->
	<form class='update-quantity-form'>
	   <?= "<div class='product-id' style='display:none;'>{$id}</div>" ?>
		<div class='input-group'>
		<?= "<input type='number' name='quantity' value='{$quantity}' class='form-control cart-quantity' min='1'>" ?>
		<span class='input-group-btn'>
		<button class='btn btn-default update-quantity' type='submit'>Update</button>
		</span>
		</div>
	</form>

	   <!-- delete from cart -->
	 	<?= "<a href='remove_from_cart.php?id={$id}' class='btn btn-default'>" ?>
		Delete
		</a>
　</div>  <!-- class='col-md-8'-->

	<div class = "col-md-4">
	<?= "<h4>&#36;" . number_format($price, 2, '.' ,',') . "</h4>"; //&#36は$を表す ?>
    </div> <!-- class = "col-md-4" -->
</div><!-- class='cart-row' -->

<?php
$item_count += $quantity;
$total += $sub_total;
} // while($row=$stmt・・
?>

<div class='col-md-8'></div>
<div class='col-md-4'>
	<div class='cart-row'>
	<?= "<h4 class='m-b-10px'>Total ({$item_count} items)	</h4>"; ?>
	<?= "<h4>&#36;" . number_format($total, 2, '.' ,',') . "</h4>"; ?>
	<a href='checkout.php' class='btn btn-success m-b-10px'>
	<span class='glyphicon glyphicon-shopping-cart'></span>Process to Checkout
	</a>
 	</div> <!-- class='cart-row' -->
</div> <!-- class='col-md-4' -->

<?php
} else {
?>	
<div class='col-md-12'>
	<div class='alert alert-danger'>
		<P>"No products found in your cart!"</P>
	</div>
</div>

<?php
} // else
// layout footer
include 'layout_foot.php';

