<?php
// connect to database
include '../config/database.php';
?>

<!DOCTYPE HTML>
<html lang="ja">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>管理者ログイン</title>

    <!-- Bootstrap読み込み（スタイリングのため） -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

        <!-- custom css for users -->
     <link href="../libs/css/user.css" rel="stylesheet" media="screen">
</head>
<body>
<!-- navbar -->
<div class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../products.php">XYZ Webstore</a>
        </div>

        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav"> 
                 <li class="active">
                    <a href="../products.php">Users page</a>
                </li>                         
            </ul>
        </div>
    </div>
</div>  <!-- navbarここまで -->

<?php

// get database connection
$database = new Database();
$db = $database->getConnection();

// include objects
include_once "../objects/adminuser.php";

$adminuser = new Adminuser($db);

//ユーザー認証機能
if(isset($_POST['user_name'], $_POST['user_password'])) {
    $adminuser->user_name = $_POST['user_name'];
    $adminuser->user_password = $_POST['user_password'];    
    if($adminuser->validate_credentials() === true) {
        header("Location: products.php");
    } else {
        echo "ユーザー名とパスワードが一致しません。";
    }
} // if(isset..) 
?>

<div class="col-xs-6 col-xs-offset-3">
<form method="post">
    <h1>管理者ログイン</h1>
    <div class="form-group">
        <input type="text" name="user_name" placeholder="ユーザー名">
    </div>
    <div class="form-group">
        <input type="password" name="user_password" placeholder="パスワード">
    </div>
    <input type="submit" value="ログイン">
</form>
</div>　<!-- class="col-xs-6 col-xs-offset-3" -->

</body>
</html>


