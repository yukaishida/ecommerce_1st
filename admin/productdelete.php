<?php
// read_products_template.phpから飛んでくる
// include classes
include_once "../config/database.php";
include_once "../objects/product.php";

// get database connection
$database = new Database();
$db = $database->getConnection();

// include page header HTML
include_once 'layout_head.php';

// initialize objects
$product = new Product($db);

// get ID of the product to be edited and action
$id = isset($_GET['id']) ? $_GET['id'] : die('ERROR: missing ID.');

// set the id as product id property
$product->id = $id;

// to read single record product
$product->readOne();

// set page title
$page_title = $product->name;

// UpdateがPOSTされたときに下記を実行
    if($product->delete() === true) {
    echo "<div class='alert alert-success' role='alert'><p>{$page_title} was Deleted!</p></div>";
    } else {
    echo "<div class='alert alert-danger' role='alert'>cannot Delete</p></div>";
    } // else   

// include page footer HTML
include_once '../layout_foot.php';