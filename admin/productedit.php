<?php
// read_products_template.phpから飛んでくる
// include classes
include_once "../config/database.php";
include_once "../objects/product.php";
include_once "../objects/product_image.php";

// get database connection
$database = new Database();
$db = $database->getConnection();

// initialize objects
$product = new Product($db);
$product_image = new ProductImage($db);

// get ID of the product to be edited and action
$id = isset($_GET['id']) ? $_GET['id'] : die('ERROR: missing ID.');

// set the id as product id property
$product->id = $id;

// set page title
$page_title = $product->name;

// include page header HTML
include_once 'layout_head.php';

// to read single record product
$product->readOne();


// UpdateがPOSTされたときに下記を実行
if(isset($_POST['update'])) {
    $product->name = $_POST['name'];
    $product->price = $_POST['price'];
    $product->description = $_POST['description'];

    if($product->update() === true) {
    echo "<div class='alert alert-success' role='alert'><p>Updated!</p></div>";
    } else {
    echo "<div class='alert alert-danger' role='alert'>cannot update</p></div>";
    } // else 
 } 

?>

<!-- 10.9 Display product details -->
<div class='col-md-5'>
    <form action="" method="post">
      <P><span class='product-detail'>Name:</span><br><textarea name="name" rows="3" cols="60"><?= $product->name ?></textarea></P>
      <P><span class='product-detail'>Price:</span><br><?= "<span class='m-b-10px price-description'>&#36</span>"?><input type="text" name="price" value="<?= number_format($product->price, 2, '.', ',') ?>"
      ></P>
      <?php
      // prepare description
      $page_description = htmlspecialchars_decode(htmlspecialchars_decode($product->description));
      ?>
      <P><span class='product-detail'>Product description:</span><br><textarea name="description" rows="10" cols="60"><?= $product->description ?></textarea></P>

      <p>
       <input type="submit" name="update" value="Update">
      </p>
    </form>
</div>

<?php
// set product id
$product_image->product_id = $id;

// read all related product image
$stmt_product_image = $product_image->readByProductId();

// count all related product image
$num_product_image = $stmt_product_image->rowCount();
?>

<div class='col-md-1'>
<?php
	// if count is more than zero
	if ($num_product_image >0) {
		// loop through all product image
	 while ($row = $stmt_product_image->fetch(PDO::FETCH_ASSOC)) {
	 	// image name and source url
	 	$product_image_name = $row['name'];
	 	$source = "../uploads/images/{$product_image_name}";
	 	echo "<img src='{$source}' class='product-img-thumb' data-img-id='{$row['id']}'>";
	 } // while
	 }	else {
	 	echo "No images.";
	 }
 ?>
</div>

<div class='col-md-4' id='product-img'>
<?php
// read all related product image
$stmt_product_image = $product_image->readByProductId();
$num_product_image = $stmt_product_image->rowCount();

// if count is more than zero
if($num_product_image > 0) {
	// loop through all product images
	$x = 0;
	while ($row=$stmt_product_image->fetch(PDO::FETCH_ASSOC)){
		// image name and source code
		$product_image_name = $row['name'];
		$source = "../uploads/images/{$product_image_name}";
		$show_product_img = $x ==0? "display-block" : "display-none";
		echo "<a href='{$source}' target='_blank' id='product-img-{$row['id']}' class='product-img {$show_product_img}'>";
		echo "<img src='{$source}' style='width:100%;' >";
		echo "</a>";
		$x++;
	} // while
} else {
	echo "No images.";
}
echo "</div>";

// include page footer HTML
include_once '../layout_foot.php';