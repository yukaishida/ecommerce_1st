<?php
// include classes
include_once "../config/database.php";
include_once "../objects/product.php";

// get database connection
$database = new Database();
$db = $database->getConnection();

// include page header HTML
include_once 'layout_head.php';

// initialize objects
$product = new Product($db);

// UpdateがPOSTされたときに下記を実行
if(isset($_POST['add'])) {
    $product->name = $_POST['name'];
    $product->price = $_POST['price'];
    $product->description = $_POST['description'];

    if($product->add() === true) {
    echo "<div class='alert alert-success' role='alert'><p>Added!</p></div>";
    } else {
    echo "<div class='alert alert-danger' role='alert'>cannot Add</p></div>";
    } // else 
 } 

?>

<!-- 10.9 Display product details -->
<div class='col-md-5'>
    <form action="" method="post">
      <P><span class='product-detail'>Name:</span><br><textarea name="name" rows="3" cols="60"></textarea></P>
      <P><span class='product-detail'>Price:</span><br><?= "<span class='m-b-10px price-description'>&#36</span>"?><input type="text" name="price" value=""
      ></P>
      <P><span class='product-detail'>Product description:</span><br><textarea name="description" rows="10" cols="60"></textarea></P>

      <p>
       <input type="submit" name="add" value="Add">
      </p>
    </form>
</div>

<?php
// include page footer HTML
include_once '../layout_foot.php';