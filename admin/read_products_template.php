<?php
// products.phpから読み込まれる
// $stmt=$product->readがproducts.phpから渡される
while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	// readメソッドで取得したid, name, description, priceのそれぞれの値をextractで取り出す
	extract($row);
?>
	<!-- creating box -->
	<div class='col-md-4 m-b-20px'>
		
		<?php
		// select and show first product image
		$product_image->product_id = $id;
		$stmt_product_image = $product_image->readFirst();

		while ($row_product_image = $stmt_product_image->Fetch(PDO::FETCH_ASSOC)) {
			echo "<div class='m-b-10px'>";
			echo "<img src='../uploads/images/{$row_product_image['name']}' class='w-100-pct' >";
			echo "</div>"; // class='m-b-10px'
		}

		// product name
		echo "<div class='product-name m-b-10px'>{$name}</div>";

		// product price and category name
		echo "<div class='m-b-10px'>";
		echo "&#36;" . number_format($price, 2,'.', ',');
		echo  "</div>"; // class='m-b-10px'

		// 修正ページ(product.php)へのリンク
		echo "<a href='productedit.php?id={$id}' class='product-link' >修正　</a>";

		// 削除ページ(productdelete.php)へのリンク
		echo "<a href='productdelete.php?id={$id}' class='product-link' >削除</a>";		

	echo "</div>";	// class='col-md-4 m-b-20px'
} // while($row = $stmt->fetch・・)
include_once "../paging.php";