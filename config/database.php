<?php
// use to get mysql database connection
class Database{
	// specify your own database credentials

	private $host = "localhost";
	private $dbname = "shop_cart_mysql_1";
	private $username = "dbuser";
	private $password = "password";
	private $conn;

	// get the database connection
	public function getConnection() {
		$this->conn = null;

		try{
			$this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->dbname, $this->username, $this->password);
		} catch (PDOException $exception){
			echo "Connection error:" . $exception->getMessage();
		}
		return $this->conn;
	} // getConnection
} // class Database


session_start();